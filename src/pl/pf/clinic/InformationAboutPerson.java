package pl.pf.clinic;

public class InformationAboutPerson
{
	public static String array[] = new String[5];

	public static boolean ifCorrectData()
	{
		for (int i = 0; i < 5; i++)
			if (array[i].isEmpty())
				return false;
		if (array[2].length() != 11)
			return false;

		if (array[0].matches(".*\\d+.*") || array[1].matches(".*\\d+.*") || !array[2].matches(".*\\d+.*")
				|| !array[4].matches(".*\\d+.*"))
			return false;

		return true;
	}

	public static void addInfo(String a, String b, String c, String d, String e)
	{
		array[0] = a;
		array[1] = b;
		array[2] = c;
		array[3] = d;
		array[4] = e;
	}
}
