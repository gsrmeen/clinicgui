package pl.pf.clinic;

import java.awt.EventQueue;
import java.util.Scanner;

public class RunTheProgram
{

	public static void main(String[] args)
	{
		new Patient("Patryk", "Fijalkowski", "97081106055", "Warszawa", "668579898");
		new Patient("Robert", "Kwiatkowski", "64042406055", "Krak�w", "602029685");
		new Patient("Krzysztof", "Kowalski", "07071109325", "Pozna�", "509892149");
		
		Doctor.addDoctor("Hugh", "Laurie", "59061139122", "Oxford", "830293075", "mrKnowItAll", 1000);
		Doctor.addDoctor("Elzbieta", "Fijalkowska", "69080739122", "Warszawa", "660205070", "Otorynolaryngolog", 150);
		EventQueue.invokeLater(new Runnable()
		{
			
			@Override
			public void run()
			{
				new MainMenuFrame();
			}
		});
	}
	



}
