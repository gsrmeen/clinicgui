package pl.pf.clinic;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class MainMenuFrame extends JFrame
{
	private JButton arrayOfButtons[][] =
	{
			{ new JButton("Add a doctor."), new JButton("List all doctors.") },
			{ new JButton("Add a patient."), new JButton("List all patients.") },
			{ new JButton("Add a visit."), new JButton("Close the program.") } };

	public MainMenuFrame()
	{
		super("MENU");
		setFramesParametres();
		addButtons();
		setButtonFunctions();
	}

	public void setButtonFunctions()
	{
		arrayOfButtons[0][0].addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				new AddDoctorFrame();

			}
		});
		arrayOfButtons[2][1].addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				dispose();

			}
		});
		arrayOfButtons[1][0].addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{

				new AddPatientFrame();

			}
		});
		arrayOfButtons[0][1].addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				Doctor.listAllDoctors();
				new ListFrame(Doctor.getNumberOfDoctors(), true);

			}
		});
		arrayOfButtons[1][1].addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				Patient.listAllPatients();
				new ListFrame(Patient.getNumberOfPatients(), false);
			}
		});
	}

	private void setFramesParametres()
	{
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(600, 600);
		setLayout(new GridLayout(3, 2));
	}

	private void addButtons()
	{
		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 2; j++)
			{
				add(arrayOfButtons[i][j]);
			}
		}

	}

}
