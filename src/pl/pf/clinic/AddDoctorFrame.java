package pl.pf.clinic;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JTextField;

public class AddDoctorFrame extends AddPersonFrame
{

	protected void addLabelsAndTextfields()
	{
		labels.add(new JLabel("Specialisation:"));
		labels.add(new JLabel("Consultancy fee:"));
		for (int i = 0; i < 7; i++)
		{
			labels.get(i).setHorizontalAlignment(JLabel.CENTER);
			add(labels.get(i));
			textfields.add(new JTextField());
			textfields.get(i).setFont(new Font("SansSerif", Font.PLAIN, 24));
			add(textfields.get(i));
		}
	}

	protected void setGetDataButton()
	{
		getDataButton.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				String usersInput[] = new String[7];
				for (int i = 0; i < 7; i++)
				{
					usersInput[i] = textfields.get(i).getText();
				}
				InformationAboutPerson.addInfo(usersInput[0], usersInput[1], usersInput[2], usersInput[3], usersInput[4]);
				if(InformationAboutPerson.ifCorrectData())
				{
					Doctor.addDoctor(usersInput[0], usersInput[1], usersInput[2], usersInput[3], usersInput[4], usersInput[5], Double.parseDouble(usersInput[6]));
					showNotificationFrame("Successfully added a doctor!");
					System.out.println("added a doctor!!! "+usersInput[0]+ " "+usersInput[1]);
					dispose();
				}
				else
					showNotificationFrame("Incorrect data!");
					

			}
		});
	}
	
	
	public AddDoctorFrame()
	{
		super();
		setGetDataButton();
	}
}
