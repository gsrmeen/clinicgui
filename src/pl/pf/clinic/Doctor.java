package pl.pf.clinic;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;

public class Doctor extends Person
{
	private int doctorsId;
	private static int numberOfDoctors = 0;
	private String specialisation;
	private double priceOfVisit;
	public static List <Doctor> listOfDoctors = new ArrayList <Doctor>();

	
	
	public static void addDoctor(String name, String surname, String PESEL, String placeOfResidecne, String phoneNumber, String specialisation, double priceOfVisit)
	{
		new Doctor(name, surname, PESEL, placeOfResidecne, phoneNumber, specialisation, priceOfVisit);
	}
	
	public static void listAllDoctors()
	{
		for(Doctor i : listOfDoctors)
			System.out.println(i);
	}
	
	private Doctor(String name, String surname, String PESEL, String placeOfResidence, String phoneNumber, String specialisation, double priceOfVisit)
	{
		super(name, surname, PESEL, placeOfResidence, phoneNumber);
		doctorsId=++numberOfDoctors;
		this.specialisation = specialisation;
		this.priceOfVisit = priceOfVisit;
		listOfDoctors.add(this);
	}
	
	@Override
	public String toString()
	{
		return (doctorsId+" "+getName()+" "+getSurname()+ " age: "+getAge());
	}
	

	public String getSpecialisation()
	{
		return specialisation;
	}

	public void setSpecialisation(String specialisation)
	{
		this.specialisation = specialisation;
	}

	public double getPriceOfVisit()
	{
		return priceOfVisit;
	}

	public void setPriceOfVisit(double priceOfVisit)
	{
		this.priceOfVisit = priceOfVisit;
	}

	public int getDoctorsId()
	{
		return doctorsId;
	}

	public static int getNumberOfDoctors()
	{
		return numberOfDoctors;
	}
	public JLabel getJLabel()
	{
		String a = getDoctorsId()+ " "+getName()+" "+getSurname()+" "+getSpecialisation()+" age:"+getAge()+ " "+getPhoneNumber();
		return new JLabel(a);
	}
}
