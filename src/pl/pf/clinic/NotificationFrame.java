package pl.pf.clinic;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class NotificationFrame extends JFrame
{
	
	public void setParameters()
	{
		setVisible(true);
		setSize(300, 75);
		setBackground(new Color(179, 255, 153));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	
	public NotificationFrame(String notification)
	{
		super("Result");
		setParameters();
		JLabel label = new JLabel(notification);
		label.setHorizontalAlignment(JLabel.CENTER);
		label.setVerticalAlignment(JLabel.CENTER);
		add(label);
		
	}
}
