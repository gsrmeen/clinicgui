package pl.pf.clinic;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Person
{
	private String name;
	private String surname;
	private String PESEL;
	private String placeOfResidence;
	private String phoneNumber;
	private boolean ifAdult;

	private int age;

	public LocalDate getDateOfBirth()
	{
		LocalDate today = LocalDate.now();
		int currentYear = today.getYear();
		currentYear -= 2000;
		int yy = Integer.parseInt(this.getPESEL().substring(0, 2));
		int mm = Integer.parseInt(this.getPESEL().substring(2, 4));
		int dd = Integer.parseInt(this.getPESEL().substring(4, 6));

		if (yy > currentYear)
			yy += 1900;
		else
			yy += 2000;
		LocalDate d = LocalDate.of(yy, mm, dd);
		return d;
	}

	public void printPerson()
	{
		System.out.println(name + " " + surname + " " + getDateOfBirth().getYear() + " " + age);
	}

	public void setAgeAndAdulty()
	{
		LocalDate today = LocalDate.now();
		this.age = today.getYear() - this.getDateOfBirth().getYear();
		if (this.age <= 18)
			this.ifAdult = false;
		else
			this.ifAdult = true;
	}
	
	public Person(String name, String surname, String PESEL, String placeOfResidecne, String phoneNumber)
	{
		this.name = name;
		this.surname = surname;
		this.PESEL = PESEL;
		this.placeOfResidence = placeOfResidecne;
		this.phoneNumber = phoneNumber;
		this.setAgeAndAdulty();
	}

	@Override
	public String toString()
	{
		return name+" "+surname+" "+PESEL+" age: "+age+" "+placeOfResidence;
	}


	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getSurname()
	{
		return surname;
	}

	public void setSurname(String surname)
	{
		this.surname = surname;
	}

	public String getPESEL()
	{
		return PESEL;
	}

	public void setPESEL(String pESEL)
	{
		PESEL = pESEL;
	}

	public String getPlaceOfResidence()
	{
		return placeOfResidence;
	}

	public void setPlaceOfResidence(String placeOfResidence)
	{
		this.placeOfResidence = placeOfResidence;
	}

	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}

	public boolean isIfAdult()
	{
		return ifAdult;
	}

	public void setIfAdult(boolean ifAdult)
	{
		this.ifAdult = ifAdult;
	}

	public int getAge()
	{
		return age;
	}

	public void setAge(int age)
	{
		this.age = age;
	}

}
