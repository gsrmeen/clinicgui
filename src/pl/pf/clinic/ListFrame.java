package pl.pf.clinic;

import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class ListFrame extends JFrame
{
	private void setFramesParametres(int numberOfPeople)
	{
		setVisible(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(600, 300);
		setLayout(new GridLayout(numberOfPeople, 1));
	}
	
	public ListFrame(int numberOfPeople, boolean ifDoctors)
	{
		super("List");
		setFramesParametres(numberOfPeople);
		if(ifDoctors)
		{
			for(Doctor d : Doctor.listOfDoctors)
			{
				JLabel label = d.getJLabel();
				label.setFont(new Font("Monospaced", Font.PLAIN, 15));
				label.setHorizontalAlignment(JLabel.CENTER);
				add(label);
			}

		}
		else
		{
			for(Patient p : Patient.listOfPatients)
			{
				JLabel label = p.getJLabel();
				label.setFont(new Font("Monospaced", Font.PLAIN, 15));
				label.setHorizontalAlignment(JLabel.CENTER);
				add(label);
			}
		}
		
	}
}
