package pl.pf.clinic;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;

public class Patient extends Person
{
	private int patientsId;
	private static int numberOfPatients = 0;
	public static List<Patient> listOfPatients = new ArrayList<Patient>();

	public static int getNumberOfPatients()
	{
		return numberOfPatients;
	}

	public Patient(String name, String surname, String PESEL, String placeOfResidecne, String phoneNumber)
	{
		super(name, surname, PESEL, placeOfResidecne, phoneNumber);
		this.patientsId = ++numberOfPatients;
		listOfPatients.add(this);
	}

	public int getPatientsId()
	{
		return patientsId;
	}

	public static void listAllPatients()
	{
		for (Patient p : listOfPatients)
			System.out.println(p);
	}
	@Override
	public String toString()
	{
		return "Patient "+getPatientsId()+" "+getName()+" "+getSurname()+" "+getPESEL()+" age: "+getAge();
	}
	
	public JLabel getJLabel()
	{
		String a = getPatientsId()+ " "+getName()+" "+getSurname()+" "+getPESEL()+" age:"+getAge()+ " "+getPhoneNumber();
		return new JLabel(a);
	}
}
