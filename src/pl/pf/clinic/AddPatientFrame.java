package pl.pf.clinic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddPatientFrame extends AddPersonFrame
{
	
	protected void setGetDataButton()
	{
		getDataButton.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				String usersInput[] = new String[5];
				for (int i = 0; i < 5; i++)
				{
					
					usersInput[i] = textfields.get(i).getText();
				}
				InformationAboutPerson.addInfo(usersInput[0], usersInput[1], usersInput[2], usersInput[3], usersInput[4]);
				if(InformationAboutPerson.ifCorrectData())
				{
					new Patient(usersInput[0], usersInput[1], usersInput[2], usersInput[3], usersInput[4]);
					showNotificationFrame("Successfully added a patient!");
					System.out.println("added a patient!!! "+usersInput[0]+ " "+usersInput[1]);
					dispose();
				}
				else
					showNotificationFrame("Incorrect data!");
					

			}
		});
	}
	
	public AddPatientFrame()
	{
		super();
		setGetDataButton();
	}
	
}
