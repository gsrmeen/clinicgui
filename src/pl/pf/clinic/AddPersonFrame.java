package pl.pf.clinic;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class AddPersonFrame extends JFrame
{
	protected JButton getDataButton = new JButton("Confirm");
	protected ArrayList<JTextField> textfields = new ArrayList<JTextField>();
	protected ArrayList<JLabel> labels = new ArrayList<JLabel>()
	{
		{
			add(new JLabel("Name:"));
			add(new JLabel("Surname:"));
			add(new JLabel("Pesel:"));
			add(new JLabel("Place of residence:"));
			add(new JLabel("Phone number:"));
		}
	};

	protected void setFramesParametres()
	{
		setVisible(true);
		setSize(500, 500);
		setBackground(new Color(179, 255, 153));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLayout(new GridLayout(8, 2));
	}

	protected void addLabelsAndTextfields()
	{
		for (int i = 0; i < 5; i++)
		{
			labels.get(i).setHorizontalAlignment(JLabel.CENTER);
			add(labels.get(i));
			textfields.add(new JTextField());
			textfields.get(i).setFont(new Font("SansSerif", Font.PLAIN, 24));
			add(textfields.get(i));
		}
	}

	protected void addButtons()
	{
		JButton closeFrameButton = new JButton("Cancel");
		add(getDataButton);
		closeFrameButton.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				disposeFrame();
			}
		});
		add(closeFrameButton);
	}

	protected void showNotificationFrame(String a)
	{
		EventQueue.invokeLater(new Runnable()
		{
			
			@Override
			public void run()
			{
				new NotificationFrame(a);
			}
		});
	}

	public AddPersonFrame()
	{
		super("Add a person");
		setFramesParametres();
		addLabelsAndTextfields();
		addButtons();


	}

	private void disposeFrame()
	{
		dispose();
	}
}
